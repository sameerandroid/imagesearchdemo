package test.gomad.imagedemo.activities;

import android.os.Bundle;
import android.os.PersistableBundle;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.bumptech.glide.Glide;

import test.gomad.imagedemo.R;
import test.gomad.imagedemo.databinding.ActivityImageDetailBinding;
import test.gomad.imagedemo.entities.ImageEntity;
import test.gomad.imagedemo.viewmodel.ImageDetailViewModel;


/**
 * Activity to show Image Detail and Adding or updating omments over the image
 * Business logic is present in view model
 *
 */
public class ImageDetailActivity extends AppCompatActivity {

    private ActivityImageDetailBinding binding;
    String imageID;
    String imageURL;
    String imageName;
    ImageDetailViewModel imageDetailViewModel;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this,R.layout.activity_image_detail);
        imageDetailViewModel =new ViewModelProvider(this).get(ImageDetailViewModel.class);
        setUpUI();
    }

    /**
     * Method to setup UI components
     */
    private void setUpUI() {

        Bundle bundle = getIntent().getExtras();
        imageID = bundle.getString("imageID","0");
        imageURL = bundle.getString("imageUrl");
        imageName = bundle.getString("imageName");


        //--- Observing live data which will retun comments coreseponing the opened image
        imageDetailViewModel.getImagDetail(imageID).observe(this, new Observer<ImageEntity>() {
            @Override
            public void onChanged(ImageEntity imageEntity) {
                if(imageEntity != null)
                binding.etComments.setText(imageEntity.getComment());
            }
        });

        setTitle(imageName);
        Glide.with(this) .load(imageURL).into(binding.imageView); // Glide is used to populate Image

        //-- Submit button will Add/update the comments
        binding.btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                binding.etComments.clearFocus();
                imageDetailViewModel.addComments(binding.etComments);
            }
        });

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
