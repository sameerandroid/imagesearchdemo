package test.gomad.imagedemo.activities

import android.app.SearchManager
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.View
import dagger.hilt.android.AndroidEntryPoint
import test.gomad.imagedemo.R
import test.gomad.imagedemo.viewmodel.MainViewModel
import androidx.activity.viewModels;
import android.widget.SearchView;
import androidx.databinding.DataBindingUtil
import androidx.databinding.adapters.SearchViewBindingAdapter
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import test.gomad.imagedemo.adapters.MainAdapter
import test.gomad.imagedemo.databinding.ActivityMainBinding
import test.gomad.imagedemo.model.Data
import test.gomad.imagedemo.model.ImageResponse
import test.gomad.imagedemo.utilties.Resource

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private val mainViewModel : MainViewModel by viewModels()
    private lateinit var adapter: MainAdapter
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        setupUI()
        setupObserver()

    }

    private fun setupUI() {

        val onImageClicked: (imageUrl: Data) -> Unit = { imageUrl ->
            val intent = Intent(this, ImageDetailActivity::class.java)
            val bundle:Bundle =  Bundle();
            bundle.putString("imageID",imageUrl.id);
            bundle.putString("imageUrl",imageUrl.images.get(0).link)
            bundle.putString("imageName",imageUrl.title)
            intent.putExtras(bundle);
            startActivity(intent)
        }

        binding.rvImages.layoutManager = GridLayoutManager(this,3)
        adapter = MainAdapter(arrayListOf(),onImageClicked)
        binding.rvImages.addItemDecoration(
            DividerItemDecoration(
                binding.rvImages.context,
                (binding.rvImages.layoutManager as LinearLayoutManager).orientation
            )
        )
        binding.rvImages.adapter = adapter
    }

    private fun setupObserver() {
        mainViewModel.users.observe(this, androidx.lifecycle.Observer<Resource<ImageResponse>> { t ->
            t.data?.data?.let { renderList(it)
            binding.progressBar.visibility = View.GONE};
        })
    }

    private fun renderList(users: List<Data>) {
        adapter.addData(users)
        adapter.notifyDataSetChanged()
    }
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.main, menu)
       // Associate searchable configuration with the SearchView
        val searchManager = getSystemService(Context.SEARCH_SERVICE) as SearchManager
        val searchView =  menu.findItem(R.id.search).actionView as SearchView

        searchView.setOnQueryTextFocusChangeListener(object :SearchViewBindingAdapter.OnQueryTextSubmit,
            View.OnFocusChangeListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
               mainViewModel.loadImages(query!!)
                return false
            }

            override fun onFocusChange(p0: View?, p1: Boolean) {

            }
        })
        (searchView).apply {
            setSearchableInfo(searchManager.getSearchableInfo(componentName))
        }
        return true
    }

}
