package test.gomad.imagedemo.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide

import kotlinx.android.synthetic.main.row_images.view.*
import test.gomad.imagedemo.R
import test.gomad.imagedemo.model.Data

class MainAdapter(
    private val users: ArrayList<Data>,
    private val onImageClicked: (imageUrl: Data) -> Unit
) : RecyclerView.Adapter<MainAdapter.DataViewHolder>() {

    class DataViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(user: Data) {
            Glide.with(itemView.imageView.context)
               .load(user.images.get(0).link).into(itemView.imageView)


        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DataViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.row_images, parent, false)
        val holder = DataViewHolder(view)
        // Add the click listener on the imageView and retrieve the proper image url
        holder.itemView.imageView.setOnClickListener {
                onImageClicked.invoke(users[holder.adapterPosition])
        }
        return holder
    }


    override fun getItemCount(): Int = users.size

    override fun onBindViewHolder(holder: DataViewHolder, position: Int) =
        holder.bind(users[position])


    fun addData(list: List<Data>) {
        users.addAll(list)
    }
}