package com.mindorks.framework.mvvm.data.api


import retrofit2.Response
import test.gomad.imagedemo.model.ImageResponse

interface ApiHelper {

    suspend fun getImages(search:String): Response<ImageResponse>
}