package com.mindorks.framework.mvvm.data.api


import retrofit2.Response
import test.gomad.imagedemo.model.ImageResponse
import javax.inject.Inject

class ApiHelperImpl @Inject constructor(private val apiService: ApiService) : ApiHelper {

    override suspend fun getImages(search:String): Response<ImageResponse> = apiService.getImages(search)

}