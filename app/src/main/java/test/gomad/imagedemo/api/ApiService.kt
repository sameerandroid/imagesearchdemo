package com.mindorks.framework.mvvm.data.api


import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query
import test.gomad.imagedemo.model.ImageResponse

interface ApiService {

    @GET("/3/gallery/search//1")
    suspend fun getImages(@Query("q") search: String): Response<ImageResponse>

}