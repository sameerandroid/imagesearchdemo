package test.gomad.imagedemo.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import test.gomad.imagedemo.entities.ImageEntity;

/**
 * Dao to handle Room Db
 */
@Dao
public interface ImageDao {

    @Query("SELECT * FROM images WHERE imageID = (:imageID)")
    public ImageEntity loadAllByIds(String imageID);

    @Insert
    long insert(ImageEntity imageEntity);


    @Update
    void update(ImageEntity imageEntity);
}