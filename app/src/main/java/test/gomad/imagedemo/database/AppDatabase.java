package test.gomad.imagedemo.database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import test.gomad.imagedemo.dao.ImageDao;
import test.gomad.imagedemo.entities.ImageEntity;

@Database(entities = {ImageEntity.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {
    public abstract ImageDao imageDao();
    private static AppDatabase appDatabaseInstance;
    private static final int DATABASE_VERSION = 1;
    private static String DATABASE_NAME = "images";

    public static synchronized AppDatabase getInstance(Context context) {
        if (appDatabaseInstance == null ) {
            appDatabaseInstance = Room.databaseBuilder(context.getApplicationContext(), AppDatabase.class, DATABASE_NAME)
                     .allowMainThreadQueries().build();

        }


        return appDatabaseInstance;
    }
}