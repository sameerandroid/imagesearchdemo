package test.gomad.imagedemo.model


data class ImageResponse (

	val data : List<Data>,
	val success : Boolean,
	val status : Int
)