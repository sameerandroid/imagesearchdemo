package test.gomad.imagedemo.repositories;

import android.app.Application;

import androidx.lifecycle.MutableLiveData;

import test.gomad.imagedemo.dao.ImageDao;
import test.gomad.imagedemo.database.AppDatabase;
import test.gomad.imagedemo.entities.ImageEntity;

/**
 * Repository class to perform CRUD operations
 */

public class ImageRepository {
    private Application application;
    public ImageRepository(Application application) {
        this.application = application;
    }

    /**
     * @return
     */
    public MutableLiveData<ImageEntity> getImageDetail(String id) {
        MutableLiveData<ImageEntity> imageEntityMutableLiveData = new MutableLiveData<>();
        AppDatabase appDatabase = AppDatabase.getInstance(application);
        ImageDao imageDao = appDatabase.imageDao();
        ImageEntity imageEntity = imageDao.loadAllByIds(id);
        imageEntityMutableLiveData.setValue(imageEntity);
        return imageEntityMutableLiveData;
    }

    public void addComments(ImageEntity imageEntity){
        AppDatabase appDatabase = AppDatabase.getInstance(application);
        ImageDao imageDao = appDatabase.imageDao();

        ImageEntity mImageEntity = imageDao.loadAllByIds(imageEntity.getImageID());
        if(mImageEntity!= null) {
            mImageEntity.setComment(imageEntity.getComment());
            imageDao.update(mImageEntity);
            return;
        }

        imageDao.insert(imageEntity);
    }

}
