package test.gomad.imagedemo.repositories

import com.mindorks.framework.mvvm.data.api.ApiHelper
import javax.inject.Inject

/**
 * Repository class to perform CRUD operations
 */
class MainRepository @Inject constructor(private val apiHelper: ApiHelper) {

    suspend fun getImages(search:String) =  apiHelper.getImages(search)

}