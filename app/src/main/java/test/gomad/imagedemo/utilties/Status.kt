package test.gomad.imagedemo.utilties

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}