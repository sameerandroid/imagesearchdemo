package test.gomad.imagedemo.viewmodel;

import android.app.Activity;
import android.app.Application;
import android.widget.EditText;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import test.gomad.imagedemo.entities.ImageEntity;
import test.gomad.imagedemo.repositories.ImageRepository;

/**
 * View model class used to interact with UI and model layer
 */
public class ImageDetailViewModel extends AndroidViewModel {

    ImageRepository imageRepository; // Repo which wil interact with Android Room
    Application application;
    String imageID;

    public ImageDetailViewModel(Application application){
        super(application);
        this.application = application;
        imageRepository = new ImageRepository(application);
    }

    /**
     *
     * @param imageID  Image id to read comments for
     * @return
     */
    public LiveData<ImageEntity> getImagDetail(String imageID){
        this.imageID = imageID;
        return imageRepository.getImageDetail(imageID);
    }


    /**
     * Method to save Comments corresponding to image
     * @param etComment
     */
    public void addComments(EditText etComment){
      if(etComment.getText().toString().length()  > 0){
            ImageEntity imageEntity = new ImageEntity();
            imageEntity.setComment(etComment.getText().toString());
            imageEntity.setImageID(imageID);
            imageRepository.addComments(imageEntity);
      }
    }

}
