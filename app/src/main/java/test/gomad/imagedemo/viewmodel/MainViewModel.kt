package test.gomad.imagedemo.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope

import kotlinx.coroutines.launch
import test.gomad.imagedemo.repositories.MainRepository
import androidx.hilt.lifecycle.ViewModelInject
import test.gomad.imagedemo.model.ImageResponse
import test.gomad.imagedemo.utilties.NetworkHelper
import test.gomad.imagedemo.utilties.Resource

class MainViewModel @ViewModelInject constructor(
    private val mainRepository: MainRepository,
    private val networkHelper: NetworkHelper
) : ViewModel() {

    private val _users = MutableLiveData<Resource<ImageResponse>>()
    val users: LiveData<Resource<ImageResponse>>
        get() = _users

    init {
        loadImages("test");
    }

    public fun loadImages(text: String) {



        viewModelScope.launch {
            _users.postValue(Resource.loading(null))
            if (networkHelper.isNetworkConnected()) {
                mainRepository.getImages(text).let {
                    if (it.isSuccessful) {
                        _users.postValue(Resource.success(it.body()))
                    } else _users.postValue(Resource.error(it.errorBody().toString(), null))
                }
            } else _users.postValue(Resource.error("No internet connection", null))
        }
    }



}